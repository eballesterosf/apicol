//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req,res) {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.get('/clientes/:idcliente', function(req, res) {
  res.send("Aqui tiene al cliente número " + req.params.idcliente);
});

var movimientosJSON = require('./movimientosv1.json');
app.get('/v1/movimientos', function(req,res){
  res.send(movimientosJSON);
});

app.post('/', function(req,res) {
  res.send("Hemos recibido su peticion post");
})

app.put('/', function(req,res) {
  res.send("Hemos recibido su peticion put cambiada");
})

app.delete('/', function(req,res) {
  res.send("Hemos recibido su peticion delete");
})
